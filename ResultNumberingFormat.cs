/*
 * The base library for ProbeNet exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Internationalization;
using System.ComponentModel;

namespace ProbeNet.Export
{
    /// <summary>
    /// Format of result numbering.
    /// </summary>
    public enum ResultNumberingFormat
    {
        /// <summary>
        /// Inactive results are hidden. There is no effect on numbering.
        /// </summary>
        [Description("hideValues")]
        [TranslatableCaption(Constants.UiDomain, "Hide inactive results")]
        HideValues,

        /// <summary>
        /// Inactive results are hidden, but numbering is contiued.
        /// </summary>
        [Description("hideValuesContinueNumbering")]
        [TranslatableCaption(Constants.UiDomain, "Hide inactive results but continue numbering", "Hide results, continue numbering")]
        HideValuesContinueNumbering,

        /// <summary>
        /// Inactive results are printed strikestrough.
        /// </summary>
        [Description("printValuesStrikethrough")]
        [TranslatableCaption(Constants.UiDomain, "Show inactive results strikethrough")]
        PrintValuesStrikethrough
    }
}

