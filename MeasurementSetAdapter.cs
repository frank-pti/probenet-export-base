using Newtonsoft.Json;
/*
 * The base library for ProbeNet exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using ProbeNet.Messages.Interface;
using ProbeNet.Messages.SerializationWrapper;
using System.Collections.Generic;
using YAXLib;

namespace ProbeNet.Export
{
    /// <summary>
    /// Measurement set adapter.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("measurementSet")]
    public class MeasurementSetAdapter
    {
        private DataSourceDataModelBase model;
        private IMeasurementDescription measurementDescription;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.MeasurementSetAdapter"/> class.
        /// </summary>
        /// <param name="model">Model.</param>
        /// <param name="description">Description.</param>
        public MeasurementSetAdapter(DataSourceDataModelBase model, IMeasurementDescription description)
        {
            Measurements = new Dictionary<MeasurementAlignmentIdentifier, MeasurementAlignmentSetAdapter>();
            MeasurementDescription = description;
            Model = model;
        }

        /// <summary>
        /// Gets the measurement description.
        /// </summary>
        /// <value>The measurement description.</value>
        [JsonProperty("description")]
        [YAXSerializableField]
        [YAXSerializeAs("description")]
        public IMeasurementDescription MeasurementDescription
        {
            get
            {
                return measurementDescription;
            }
            private set
            {
                measurementDescription = new MeasurementDescription(value);
            }
        }

        /// <summary>
        /// Gets the measurements.
        /// </summary>
        /// <value>The measurements.</value>
        [YAXDontSerialize]
        public IDictionary<MeasurementAlignmentIdentifier, MeasurementAlignmentSetAdapter> Measurements
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the measurements for export
        /// </summary>
        /// <value>The measurement for export.</value>
        [JsonProperty("measurements")]
        [YAXSerializableField]
        [YAXCollection(YAXCollectionSerializationTypes.RecursiveWithNoContainingElement, EachElementName = "measurement")]
        public IList<IMeasurement> MeasurementsForExport
        {
            get
            {
                List<IMeasurement> measurements = new List<IMeasurement>();
                foreach (MeasurementAlignmentSetAdapter adapter in Measurements.Values) {
                    foreach (IMeasurement measurement in adapter.Measurements) {
                        measurements.Add(new Measurement(measurement));
                    }
                }
                return measurements;
            }
        }

        /// <summary>
        /// Gets the visible results.
        /// </summary>
        /// <value>The visible results.</value>
        [JsonProperty("visibleResults")]
        [YAXSerializableField]
        [YAXCollection(YAXCollectionSerializationTypes.Recursive, EachElementName = "measurement")]
        [YAXSerializeAs("visibleResults")]
        public IList<VisibleResultList> VisibleResults
        {
            get
            {
                IList<VisibleResultList> result = new List<VisibleResultList>();
                foreach (KeyValuePair<string, IList<string>> pair in model.VisibleResultColumns) {
                    result.Add(new VisibleResultList(pair.Key, pair.Value));
                }
                return result;
            }
        }

        /// <summary>
        /// Gets the measurment metadata.
        /// </summary>
        /// <value>The meta data.</value>
        [JsonProperty("metadata")]
        [YAXSerializableField]
        [YAXSerializeAs("metadata")]
        public IMeasurementMetadata Metadata
        {
            get
            {
                return Model.MeasurementMetadata[MeasurementDescription.Id];
            }
        }

        [JsonProperty("textualAttributes")]
        [YAXSerializableField]
        [YAXSerializeAs("textualAttributes")]
        [YAXDictionary(EachPairName = "attribute", KeyName = "name", ValueName = "value",
            SerializeKeyAs = YAXNodeTypes.Attribute, SerializeValueAs = YAXNodeTypes.Element)]
        public IDictionary<string, string> TextualAttributes
        {
            get
            {
                Dictionary<string, string> result = new Dictionary<string, string>();
                foreach (Parameter parameter in model.TextualAttributes) {
                    result.Add(parameter.Name, parameter.Value);
                }
                return result;
            }
        }

        /// <summary>
        /// Gets the custom measurement parameters.
        /// </summary>
        /// <value>The custom measurement parameters.</value>
        [YAXSerializableField]
        [YAXSerializeAs("customMeasurementParameters")]
        [YAXCollection(YAXCollectionSerializationTypes.Recursive, EachElementName = "parameter")]
        [JsonIgnore]
        public ParameterList CustomMeasurementParameters
        {
            get
            {
                if (Model.CustomMeasurementParameters == null) {
                    return new ParameterList();
                }
                if (!model.CustomMeasurementParameters.ContainsKey(MeasurementDescription.Id)) {
                    return new ParameterList();
                }
                return Model.CustomMeasurementParameters[MeasurementDescription.Id];
            }
        }

        /// <summary>
        /// Gets the custom measurement parameters for JSON export.
        /// </summary>
        /// <value>The custom measurement parameters for JSON.</value>
        [JsonProperty("customMeasurementParameters")]
        [YAXDontSerialize]
        public IDictionary<string, string> CustomMeasurementParametersForJson
        {
            get
            {
                return Model.BuildCustomMeasurementParameters(MeasurementDescription.Id);
            }
        }

        /// <summary>
        /// Gets the model.
        /// </summary>
        /// <value>The model.</value>
        [YAXDontSerialize]
        public DataSourceDataModelBase Model
        {
            get
            {
                return model;
            }
            private set
            {
                model = value;
                IDictionary<MeasurementAlignmentIdentifier, IList<IMeasurement>> measurement =
                    model.Measurements[MeasurementDescription.Id];
                foreach (MeasurementAlignmentIdentifier identifier in measurement.Keys) {
                    AddMeasurementAlignmentIdentifier(identifier);
                }
                model.SequenceAdded += HandleSequenceAdded;
            }
        }

        private void HandleSequenceAdded(DataSourceDataModelBase sender, ISequenceDescription description, ISequence sequence)
        {
            IList<IMeasurement> measurements = MeasurementDescription.ExtractMeasurements(description, sequence, null);
            foreach (IMeasurement measurement in measurements) {
                MeasurementAlignmentIdentifier identifier =
                    new MeasurementAlignmentIdentifier(MeasurementDescription, measurement.Alignment);
                AddMeasurementAlignmentIdentifier(identifier);
            }
        }

        private void AddMeasurementAlignmentIdentifier(MeasurementAlignmentIdentifier identifier)
        {
            if (!Measurements.ContainsKey(identifier)) {
                Measurements.Add(identifier, new MeasurementAlignmentSetAdapter(Model, identifier));
            }
        }
    }
}

