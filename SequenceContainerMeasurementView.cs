using CairoChart.Model;
using Logging;
using ProbeNet.Messages.Interface;
/*
 * The base library for ProbeNet exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System.Collections.Generic;

namespace ProbeNet.Export
{
    /// <summary>
    /// Sequence container measurement view.
    /// </summary>
    public class SequenceContainerMeasurementView : IContainer
    {
        /// <summary>
        /// Delegate void for row added event.
        /// </summary>
        public delegate void RowAddedDelegate(SequenceContainerMeasurementView sender, int index);
        /// <summary>
        /// Occurs when row added.
        /// </summary>
        public event RowAddedDelegate RowAdded;
        /// <summary>
        /// Delegate void for selected items changed event.
        /// </summary>
        public delegate void SelectedItemsChangedDelegate(SequenceContainerMeasurementView sender, int index);
        /// <summary>
        /// Occurs when selected items changed.
        /// </summary>
        public event SelectedItemsChangedDelegate SelectedItemsChanged;

        private bool hasNewData;
        private List<DefaultXYSeries<double, double>> curveSeries;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.SequenceContainerMeasurementView"/> class.
        /// </summary>
        /// <param name="model">Model.</param>
        public SequenceContainerMeasurementView(MeasurementAlignmentSetAdapter model)
        {
            Model = model;
            model.MeasurementActiveStateChanged += HandleActiveStateChanged;
            if (model.Description.CurveDescription != null) {
                curveSeries = new List<DefaultXYSeries<double, double>>();
            }
            Logger.Information("connecting to model.MeasurementsAdded: {0}", model.Identifier.MeasurementDescription.Id);
            model.MeasurementsAdded += HandleMeasurementsAdded;
            hasNewData = false;
            LoadExistingMeasurements(model);
        }

        private void HandleMeasurementsAdded(IList<IMeasurement> measurements)
        {
            foreach (IMeasurement measurement in measurements) {
                AddMeasurement(measurement);
            }
        }

        private void LoadExistingMeasurements(MeasurementAlignmentSetAdapter model)
        {
            HandleMeasurementsAdded(model.Measurements);
        }

        private void AddMeasurement(IMeasurement measurement)
        {
            hasNewData = true;

            if (curveSeries != null) {
                DefaultXYSeries<double, double> series = new DefaultXYSeries<double, double>(measurement.Curve, 0, measurement.Curve, 1);
                curveSeries.Add(series);
            }

            NotifyRowAdded(Model.Measurements.Count - 1);
        }

        private void HandleActiveStateChanged(IMeasurement sender, int index)
        {
            NotifySelectedItemsChanged(index);
        }

        private void NotifyRowAdded(int index)
        {
            if (RowAdded != null) {
                RowAdded(this, index);
            }
        }

        private void NotifySelectedItemsChanged(int index)
        {
            if (SelectedItemsChanged != null) {
                SelectedItemsChanged(this, index);
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has selected items.
        /// </summary>
        /// <value><c>true</c> if this instance has selected items; otherwise, <c>false</c>.</value>
        public bool HasSelectedItems
        {
            get
            {
                foreach (IMeasurement measurement in Model.Measurements) {
                    if (measurement.Active) {
                        return true;
                    }
                }
                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the measurement has diagram.
        /// </summary>
        /// <value><c>true</c> if this instance has diagram; otherwise, <c>false</c>.</value>
        public bool HasDiagram
        {
            get
            {
                return Model.Description.CurveDescription != null;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this sequence has new data.
        /// </summary>
        /// <value><c>true</c> if this instance has new data; otherwise, <c>false</c>.</value>
        public bool HasNewData
        {
            get
            {
                return hasNewData;
            }
            set
            {
                hasNewData = value;
            }
        }

        /// <summary>
        /// Gets the curve series.
        /// </summary>
        /// <value>The curve series.</value>
        public List<DefaultXYSeries<double, double>> CurveSeries
        {
            get
            {
                return curveSeries;
            }
        }

        /// <summary>
        /// Gets the selected curves.
        /// </summary>
        /// <returns>The selected curves.</returns>
        public List<DefaultXYSeries<double, double>> GetSelectedCurves()
        {
            List<DefaultXYSeries<double, double>> result = new List<DefaultXYSeries<double, double>>();
            for (int i = 0; i < Model.Measurements.Count; i++) {
                CurveSeries[i].Visible = Model.Measurements[i].Active;
                result.Add(CurveSeries[i]);
            }
            return result;
        }

        /// <summary>
        /// Gets the amount of selected items.
        /// </summary>
        /// <value>The amount of selected items.</value>
        public int SelectedItemCount
        {
            get
            {
                int count = 0;
                foreach (IMeasurement measurement in Model.Measurements) {
                    if (measurement.Active) {
                        count++;
                    }
                }
                return count;
            }
        }

        /// <summary>
        /// Gets the selected items.
        /// </summary>
        /// <returns>The selected items.</returns>
        public Dictionary<int, IMeasurement> GetSelectedItems()
        {
            return GetItems(true);
        }

        /// <summary>
        /// Gets the inactive items.
        /// </summary>
        /// <returns>The inactive items.</returns>
        public Dictionary<int, IMeasurement> GetInactiveItems()
        {
            return GetItems(false);
        }

        /// <summary>
        /// Gets the items.
        /// </summary>
        /// <returns>The items.</returns>
        /// <param name="active">If set to <c>true</c> the active items, otherwise the inactive items.</param>
        public Dictionary<int, IMeasurement> GetItems(bool active)
        {
            Dictionary<int, IMeasurement> result = new Dictionary<int, IMeasurement>();
            for (int i = 0; i < Model.Measurements.Count; i++) {
                IMeasurement measurement = Model.Measurements[i];
                if (measurement.Active == active) {
                    result.Add(i + 1, measurement);
                }
            }
            return result;
        }

        /// <summary>
        /// Gets the measurement description.
        /// </summary>
        /// <value>The measurement description.</value>
        public IMeasurementDescription MeasurementDescription
        {
            get
            {
                return Model.Description;
            }
        }

        /// <summary>
        /// Gets the model.
        /// </summary>
        /// <value>The model.</value>
        public MeasurementAlignmentSetAdapter Model
        {
            get;
            private set;
        }

        #region IContainer implementation
        int IContainer.Rows
        {
            get
            {
                return Model.Measurements.Count;
            }
        }

        int IContainer.ActiveRows
        {
            get
            {
                return GetSelectedItems().Count;
            }
        }

        int IContainer.Columns
        {
            get
            {
                return Model.Description.ResultDescriptions.Count;
            }
        }
        #endregion

        public void UpdateDescriptions()
        {
            Model.UpdateDescriptions();
        }
    }
}

