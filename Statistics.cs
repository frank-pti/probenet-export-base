/*
 * The base library for ProbeNet exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Internationalization;
using System.ComponentModel;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;

namespace ProbeNet.Export
{
    /// <summary>
    /// Class containing various statistic methods.
    /// </summary>
    public static class Statistics
    {
        internal static int DefaultPrecision = 3;

        private static IList<MethodInfo> methods;

        /// <summary>
        /// Gets the methods of this class.
        /// </summary>
        /// <returns>The methods.</returns>
        public static IList<MethodInfo> GetMethods()
        {
            if (methods == null) {
                IEnumerable<MethodInfo> methodInfos =
                    from method in typeof(Statistics).GetMethods(BindingFlags.Public | BindingFlags.Static)
                        where method.HasAttribute<DescriptionAttribute>()
                        select method;
                methods = new List<MethodInfo>(methodInfos);
            }
            return methods;
        }

        /// <summary>
        /// Calculates the maximum value of the specified values.
        /// </summary>
        /// <param name="precision">The precision of the result.</param>
        /// <param name="values">The values.</param>
        /// <returns>The maximum value.</returns>
        [TranslatableCaption(Constants.UiDomain, "Maximum")]
        [Color(0xFF, 0xFF, 0xFF)]
        [SpreadsheetFormula("MAX({0})")]
        [Description("maximum")]
        [DefaultNumericValueDisplayFormatAttribute(NumericValueDisplayFormat.Decimal)]
        public static Nullable<double> Maximum(ref int precision, params Nullable<double>[] values)
        {
            if (values.Any(d => d == null || double.IsNaN(d.Value))) {
                return null;
            }
            return Round(values.Max(), precision);
        }

        /// <summary>
        /// Calculates the average value of the specified values.
        /// </summary>
        /// <param name="precision">The precision of the result.</param>
        /// <param name="values">The values.</param>
        /// <returns>The average value.</returns>
        [TranslatableCaption(Constants.UiDomain, "Average")]
        [Color(0xDD, 0xDD, 0xFF)]
        [SpreadsheetFormula("AVERAGE({0})")]
        [Description("average")]
        [DefaultNumericValueDisplayFormatAttribute(NumericValueDisplayFormat.Decimal)]
        public static Nullable<double> Average(ref int precision, params Nullable<double>[] values)
        {
            if (values.Any(d => d == null || double.IsNaN(d.Value) ) ) {
                return null;
            }
            return Round (values.Average(), precision);
        }

        private static Nullable<double> Average(params Nullable<double>[] values)
        {
            int precision = int.MaxValue;
            return Average(ref precision, values);
        }

        /// <summary>
        /// Calculates the minimum value of the parameters.
        /// </summary>
        /// <param name="precision">The precision of the result.</param>
        /// <param name="values">The values.</param>
        /// <returns>The minimum value.</returns>
        [TranslatableCaption(Constants.UiDomain, "Minimum")]
        [Color(0xFF, 0xFF, 0xFF)]
        [SpreadsheetFormula("MIN({0})")]
        [Description("minimum")]
        [DefaultNumericValueDisplayFormatAttribute(NumericValueDisplayFormat.Decimal)]
        public static Nullable<double> Minimum(ref int precision, params Nullable<double>[] values)
        {
            if (values.Any(d => d == null || double.IsNaN(d.Value))) {
                return null;
            }
            return Round(values.Min(), precision);
        }

        /// <summary>
        /// Calculates the standard deviation value of the parameters.
        /// </summary>
        /// <param name="precision">The precision of the result.</param>
        /// <param name="values">The values.</param>
        /// <returns>The standard deviation value.</returns>
        [TranslatableCaption(Constants.UiDomain, "Standard Deviation", "StdDev")]
        [Color(0xFF, 0xFF, 0xFF)]
        [SpreadsheetFormula("STDEVP({0})")]
        [Description("standardDeviation")]
        [DefaultNumericValueDisplayFormatAttribute(NumericValueDisplayFormat.Decimal)]
        [UseDefaultPrecision]
        public static Nullable<double> StandardDeviation(ref int precision, params Nullable<double>[] values)
        {
            Nullable<double> avg = Average(values);
            if (avg == null || values.Any(d => d == null || double.IsNaN(d.Value))) {
                return null;
            }
            Nullable<double> sum = values.Sum (d => (d.Value - avg) * (d.Value - avg));
            if (sum == null) {
                return null;
            }
            return Round (Math.Sqrt(sum.Value / (values.Length - 1)), precision);
        }

        private static Nullable<double> StandardDeviation(params Nullable<double>[] values) {
            int precision = int.MaxValue;
            return StandardDeviation(ref precision, values);
        }

        /// <summary>
        /// Calculates the coefficent of variation value of the parameters.
        /// </summary>
        /// <param name="precision">The precision of the result.</param>
        /// <param name="values">The values.</param>
        /// <returns>The coefficient of variation value.</returns>
        [TranslatableCaption(Constants.UiDomain, "Coefficient of variation", "CV")]
        [Color(0xFF, 0xFF, 0xFF)]
        [SpreadsheetFormula("STDEVP({0})/AVERAGE({0})")]
        [Description("coefficientOfVariation")]
        [DefaultNumericValueDisplayFormatAttribute(NumericValueDisplayFormat.Percent)]
        public static Nullable<double> CoefficientOfVariation(ref int precision, params Nullable<double>[] values)
        {
            Nullable<double> stdDev = StandardDeviation(values);
            Nullable<double> avg = Average(values);

            precision = DefaultPrecision;

            if (stdDev == null || avg == null) {
                return null;
            }
            if (avg.Value == 0) {
                return null;
            }
            return Round(stdDev.Value / avg.Value, precision);
        }

        private static Nullable<double> Round(Nullable<double> value, int precision)
        {
            if (precision == int.MaxValue) {
                return value;
            }
            if (value == null) {
                return null;
            }
            if (precision < 0) {
                precision = 0;
            }
            if (precision > 15) {
                precision = 15;
            }
            return Math.Round(value.Value, precision);
        }
    }
}

