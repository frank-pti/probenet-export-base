using ProbeNet.Enums;
using ProbeNet.Messages.Interface;
/*
 * The base library for ProbeNet exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace ProbeNet.Export
{
    /// <summary>
    /// Measurement alignment identifier.
    /// </summary>
    public class MeasurementAlignmentIdentifier : Tuple<IMeasurementDescription, Nullable<SampleSide>, Nullable<int>>, IComparable<MeasurementAlignmentIdentifier>, IAlignment
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.MeasurementAlignmentIdentifier"/> class.
        /// </summary>
        /// <param name="measurementDescription">Measurement description.</param>
        /// <param name="alignment">Alignment.</param>
        public MeasurementAlignmentIdentifier(IMeasurementDescription measurementDescription, IAlignment alignment) :
            this(measurementDescription, GetSide(alignment), GetDirection(alignment))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.MeasurementAlignmentIdentifier"/> class.
        /// </summary>
        /// <param name="measurementDescription">Measurement description.</param>
        /// <param name="sampleSide">Sample side.</param>
        /// <param name="direction">Direction.</param>
        public MeasurementAlignmentIdentifier(IMeasurementDescription measurementDescription, Nullable<SampleSide> sampleSide, Nullable<int> direction) :
            base(measurementDescription, sampleSide, direction)
        {
        }

        private static Nullable<SampleSide> GetSide(IAlignment alignment)
        {
            if (alignment == null) {
                return null;
            }
            return alignment.Side;
        }

        private static Nullable<int> GetDirection(IAlignment alignment)
        {
            if (alignment == null) {
                return null;
            }
            return alignment.Direction;
        }

        /// <summary>
        /// Gets or sets the measurement description.
        /// </summary>
        /// <value>The measurement description.</value>
        public IMeasurementDescription MeasurementDescription
        {
            get
            {
                return Item1;
            }
            set
            {
                throw new NotSupportedException("Measurement cannot be set");
            }
        }

        /// <summary>
        /// Gets or sets the side.
        /// </summary>
        /// <value>The side.</value>
        public Nullable<SampleSide> Side
        {
            get
            {
                return Item2;
            }
            set
            {
                throw new NotSupportedException("Side cannot be set");
            }
        }

        /// <summary>
        /// Gets or sets the direction.
        /// </summary>
        /// <value>The direction.</value>
        public Nullable<int> Direction
        {
            get
            {
                return Item3;
            }
            set
            {
                throw new NotSupportedException("Direction cannot be set");
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has side.
        /// </summary>
        /// <value><c>true</c> if this instance has side; otherwise, <c>false</c>.</value>
        public bool HasSide
        {
            get
            {
                return Side != null;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has direction.
        /// </summary>
        /// <value><c>true</c> if this instance has direction; otherwise, <c>false</c>.</value>
        public bool HasDirection
        {
            get
            {
                return Direction != null;
            }
        }

        private static int Comparison(MeasurementAlignmentIdentifier a, MeasurementAlignmentIdentifier b)
        {
            if (a.Item1 != null && b.Item1 == null) {
                return 1;
            }
            if (a.Item1 == null && b.Item1 != null) {
                return -1;
            }
            if (!a.Item1.Equals(b.Item1)) {
                return 1;
            }

            if (a.Item2 != null && b.Item2 == null) {
                return 1;
            }
            if (a.Item2 == null && b.Item2 != null) {
                return -1;
            }
            if (a.Item2 != null && b.Item2 != null) {
                if (a.Item2.Value > b.Item2.Value) {
                    return 1;
                }
                if (a.Item2.Value < b.Item2.Value) {
                    return -1;
                }
            }

            if (a.Item3 != null && b.Item3 == null) {
                return 1;
            }
            if (a.Item3 == null && b.Item3 != null) {
                return -1;
            }
            if (a.Item3 != null && b.Item3 != null) {
                if (a.Item3.Value > b.Item3.Value) {
                    return 1;
                }
                if (a.Item3.Value < b.Item3.Value) {
                    return -1;
                }
            }

            return 0;
        }

        /// <summary>Tests equality</summary>
        /// <param name="a">The one measurement alignment identifier</param>
        /// <param name="b">The other measurement alignment identifier</param>
        public static bool operator ==(MeasurementAlignmentIdentifier a, MeasurementAlignmentIdentifier b)
        {
            if ((object)a == null && (object)b == null) {
                return true;
            }
            if ((object)a == null || (object)b == null) {
                return false;
            }
            if (a.MeasurementDescription.Id != b.MeasurementDescription.Id) {
                return false;
            }
            if (a.HasDirection != b.HasDirection) {
                return false;
            }
            if (a.HasSide != b.HasSide) {
                return false;
            }
            if (a.HasDirection && (a.Direction != b.Direction)) {
                return false;
            }
            if (a.HasSide && (a.Side != b.Side)) {
                return false;
            }
            return true;
        }

        /// <summary>Tests inequality</summary>
        /// <param name="a">The one measurement alignment identifier</param>
        /// <param name="b">The other measurement alignment identifier</param>
        public static bool operator !=(MeasurementAlignmentIdentifier a, MeasurementAlignmentIdentifier b)
        {
            return !(a == b);
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/> is equal to the current <see cref="ProbeNet.Export.MeasurementAlignmentIdentifier"/>.
        /// </summary>
        /// <param name="other">The <see cref="System.Object"/> to compare with the current <see cref="ProbeNet.Export.MeasurementAlignmentIdentifier"/>.</param>
        /// <returns><c>true</c> if the specified <see cref="System.Object"/> is equal to the current
        /// <see cref="ProbeNet.Export.MeasurementAlignmentIdentifier"/>; otherwise, <c>false</c>.</returns>
        public override bool Equals(object other)
        {
            if (!(other is MeasurementAlignmentIdentifier)) {
                return false;
            }
            return (MeasurementAlignmentIdentifier)other == this;
        }

        /// <summary>
        /// Serves as a hash function for a <see cref="ProbeNet.Export.MeasurementAlignmentIdentifier"/> object.
        /// </summary>
        /// <returns>A hash code for this instance that is suitable for use in hashing algorithms and data structures such as a
        /// hash table.</returns>
        public override int GetHashCode()
        {
            return MeasurementDescription.Id.GetHashCode() ^
                (HasDirection.GetHashCode() << 1) ^
                (HasSide.GetHashCode() << 2) ^
                ((HasDirection ? Direction.GetHashCode() : 0) << 3) ^
                ((HasSide ? Side.GetHashCode() : 0) << 4);
        }

        #region IComparable[MeasurementAlignmentIdentifier] implementation
        int IComparable<MeasurementAlignmentIdentifier>.CompareTo(MeasurementAlignmentIdentifier other)
        {
            return Comparison(this, other);
        }
        #endregion

        #region IEquatable[ProbeNet.Backend.IAlignment] implementation
        bool IEquatable<IAlignment>.Equals(IAlignment other)
        {
            return Direction == other.Direction && Side == other.Side;
        }
        #endregion
    }
}

