/*
 * The base library for ProbeNet exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.ComponentModel;

namespace ProbeNet.Export
{
    /// <summary>
    /// Base class for data exports.
    /// </summary>
    public abstract class DataExport
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.DataExport"/> class.
        /// </summary>
        /// <param name="settingsProvider">Settings provider.</param>
        protected DataExport (SettingsProvider settingsProvider)
        {
            if (settingsProvider == null) {
                throw new ArgumentNullException ("settingsProvider");
            }
            object[] attributes = GetType().GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (attributes.Length < 1) {
                throw new TypeLoadException(String.Format(
                    "Type {0} must have a Description attribute, but none was found.", GetType().Name));
            }
            Name = ((DescriptionAttribute)attributes[0]).Description;
            SettingsProvider = settingsProvider;
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name {
            get;
            private set;
        }

        /// <summary>Test equality of two data export objects.</summary>
        /// <param name="a">The one data export.</param>
        /// <param name="b">The the other data export.</param>
        public static bool operator == (DataExport a, DataExport b)
        {
            if (((object)a == null) && ((object)b == null)) {
                return true;
            }
            if (((object)a == null) || ((object)b == null)) {
                return false;
            }
            return a.Name == b.Name;
        }

        /// <summary>Test inequality of two data export objects.</summary>
        /// <param name="a">The one data export.</param>
        /// <param name="b">The the other data export.</param>
        public static bool operator != (DataExport a, DataExport b)
        {
            return !(a == b);
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/> is equal to the current <see cref="ProbeNet.Export.DataExport"/>.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object"/> to compare with the current <see cref="ProbeNet.Export.DataExport"/>.</param>
        /// <returns><c>true</c> if the specified <see cref="System.Object"/> is equal to the current
        /// <see cref="ProbeNet.Export.DataExport"/>; otherwise, <c>false</c>.</returns>
        public override bool Equals (object obj)
        {
            if (obj is DataExport) {
                return ((obj as DataExport) == this);
            }
            return false;
        }

        /// <summary>
        /// Serves as a hash function for a <see cref="ProbeNet.Export.DataExport"/> object.
        /// </summary>
        /// <returns>A hash code for this instance that is suitable for use in hashing algorithms and data structures such as a
        /// hash table.</returns>
        public override int GetHashCode ()
        {
            return base.GetHashCode () ^ Name.GetHashCode();
        }

        /// <summary>
        /// Gets the settings provider.
        /// </summary>
        /// <value>The settings provider.</value>
        protected SettingsProvider SettingsProvider {
            get;
            private set;
        }
    }
}
