/*
 * The base library for ProbeNet exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Newtonsoft.Json;
using YAXLib;
using System.Collections.Generic;

namespace ProbeNet.Export
{
    /// <summary>
    /// Parameter.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("customParameter")]
    public class Parameter: Tuple<string, string>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.Parameter"/> class.
        /// </summary>
        public Parameter():
            base(String.Empty, String.Empty)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.Parameter"/> class.
        /// </summary>
        /// <param name="item">Item.</param>
        public Parameter (KeyValuePair<string, string> item):
            this(item.Key, item.Value)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.Parameter"/> class.
        /// </summary>
        /// <param name="name">Name.</param>
        /// <param name="value">Value.</param>
        [JsonConstructor]
        public Parameter (string name, string value):
            base(name, value)
        {
        }

        /// <summary>
        /// The name.
        /// </summary>
        /// <value>The name.</value>
        [JsonProperty("name")]
        [YAXSerializableField]
        [YAXSerializeAs("name")]
        [YAXAttributeForClass]
        public string Name {
            get {
                return Item1;
            }
        }

        /// <summary>
        /// The value.
        /// </summary>
        /// <value>The value.</value>
        [JsonProperty("value")]
        [YAXSerializableField]
        [YAXSerializeAs("value")]
        [YAXAttributeForClass]
        public string Value {
            get {
                return Item2;
            }
        }
    }
}

