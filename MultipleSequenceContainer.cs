/*
 * The base library for ProbeNet exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using YAXLib;
using System.IO;

namespace ProbeNet.Export
{
    /// <summary>
    /// Multiple sequence container.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("probenetSet")]
    public class MultipleSequenceContainer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.MultipleSequenceContainer"/> class.
        /// </summary>
        public MultipleSequenceContainer ()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.MultipleSequenceContainer"/> class.
        /// </summary>
        /// <param name="sequenceContainers">Sequence containers.</param>
        public MultipleSequenceContainer (IList<SequenceContainer> sequenceContainers)
        {
            SequenceContainers = sequenceContainers;
        }

        /// <summary>
        /// Gets or sets the sequence containers (a "ProbeNet set").
        /// </summary>
        /// <value>The sequence containers.</value>
        [JsonProperty("probenetSet")]
        [YAXSerializableField]
        [YAXCollection(YAXCollectionSerializationTypes.RecursiveWithNoContainingElement)]
        public IList<SequenceContainer> SequenceContainers {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the name of the custom paraemters profile.
        /// </summary>
        /// <value>The name of the custom parameters profile.</value>
        [JsonProperty ("customParametersProfile")]
        [YAXSerializableField]
        [YAXSerializeAs ("profile")]
        [YAXAttributeFor ("customParametersProfile")]
        public string CustomParametersProfileName {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the custom parameters profile name should be printed.
        /// </summary>
        /// <value><c>true</c> if the custom parameters profile should be printed; otherwise, <c>false</c>.</value>
        [JsonProperty ("printCustomParametersProfileName")]
        [YAXSerializableField]
        [YAXSerializeAs ("printName")]
        [YAXAttributeFor ("customParametersProfile")]
        public bool PrintCustomParametersProfileName {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the custom parameters.
        /// </summary>
        /// <value>The custom parameters.</value>
        [JsonProperty ("customParameters")]
        [YAXSerializableField]
        [YAXSerializeAs ("customParameters")]
        [YAXDictionary (EachPairName = "parameter", KeyName = "name", SerializeKeyAs = YAXNodeTypes.Attribute, ValueName = "value", SerializeValueAs = YAXNodeTypes.Element)]
        public IDictionary<string, string> CustomParameters {
            get;
            set;
        }

        /// <summary>
        /// Serializes the stream json.
        /// </summary>
        /// <param name="writeStream">Write stream.</param>
        public void SerializeStreamJson(Stream writeStream)
        {
            TextWriter textWriter = new StreamWriter(writeStream);
            JsonTextWriter jsonWriter = new JsonTextWriter(textWriter);
            jsonWriter.Formatting = Formatting.Indented;

            try {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(jsonWriter, this);
                textWriter.Flush();
            }
            catch(Exception e)
            {
                throw e;
            }
        }
    }
}

