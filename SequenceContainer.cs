/*
 * The base library for ProbeNet exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using YAXLib;
using ProbeNet.Messages.Interface;

namespace ProbeNet.Export
{
    /// <summary>
    /// Sequence container.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("probenet")]
    public class SequenceContainer
    {
        /// <summary>
        /// Delegate method for measuremnt added event.
        /// </summary>
        public delegate void MeasurementAddedDelegate(MeasurementAlignmentIdentifier identifier, IMeasurement measurement);
        /// <summary>
        /// Occurs when measurement added.
        /// </summary>
        public event MeasurementAddedDelegate MeasurementAdded;

        private List<ISequence> sequences;
        private bool exportCurve;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.SequenceContainer"/> class.
        /// </summary>
        public SequenceContainer()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.SequenceContainer"/> class.
        /// </summary>
        /// <param name="sequenceDescription">Sequence description.</param>
        public SequenceContainer(ISequenceDescription sequenceDescription)
        {
            SequenceDescriptionForExport = sequenceDescription;
            MeasurementDescriptions = LoadMeasurementDescriptions(sequenceDescription.Measurements);
            sequences = new List<ISequence>();
            exportCurve = true;
        }

        private Dictionary<string, IMeasurementDescription> LoadMeasurementDescriptions(IList<ISequenceDescriptionMeasurementReference> references)
        {
            Dictionary<string, IMeasurementDescription> measurements = new Dictionary<string, IMeasurementDescription>();
            foreach (ISequenceDescriptionMeasurementReference reference in references)
            {
                if (!measurements.ContainsKey(reference.Measurement.Id)) {
                    measurements.Add(reference.Measurement.Id, reference.Measurement);
                }
            }
            return measurements;
        }

        /// <summary>
        /// Handles the sequence added event.
        /// </summary>
        /// <param name="sequence">Sequence to be added.</param>
        /// <param name="notifier">Notifier method to be called.</param>
        public void SequenceAdded(ISequence sequence, MeasurementAddedDelegate notifier)
        {
            foreach (string designation in sequence.Measurements.Keys) {
                IMeasurement measurement = sequence.Measurements[designation];
                IMeasurementDescription description = LoadMeasurementDescription(designation);
                MeasurementAlignmentIdentifier identifier = new MeasurementAlignmentIdentifier(description, measurement.Alignment);
                notifier(identifier, measurement);
            }
        }

        /// <summary>
        /// Adds the sequence.
        /// </summary>
        /// <param name="sequence">Sequence.</param>
        public void AddSequence(ISequence sequence)
        {
            sequences.Add (sequence);
            SequenceAdded (sequence, NotifyMeasurementAdded);
        }

        /// <summary>
        /// Gets the measurement alignments.
        /// </summary>
        /// <value>The measurement alignments.</value>
        public IList<MeasurementAlignmentIdentifier> MeasurementAlignments {
            get {
                List<MeasurementAlignmentIdentifier> result =
                    new List<MeasurementAlignmentIdentifier>();

                foreach (ISequence sequence in sequences) {
                    foreach (string measurementDesignation in sequence.Measurements.Keys) {
                        IMeasurement measurement = sequence.Measurements[measurementDesignation];
                        IMeasurementDescription measurementDescription = LoadMeasurementDescription(measurementDesignation);

                        MeasurementAlignmentIdentifier identifier = new MeasurementAlignmentIdentifier(
                            measurementDescription, measurement.Alignment);
                        if (!result.Contains (identifier)) {
                            result.Add(identifier);
                        }
                    }
                }
                result.Sort();
                return result;
            }
        }

        private IMeasurementDescription LoadMeasurementDescription(string designation)
        {
            foreach (ISequenceDescriptionMeasurementReference reference in SequenceDescriptionForExport.Measurements) {
                if (reference.Designation == designation) {
                    return reference.Measurement;
                }
            }
            throw new ArgumentException(String.Format(
                "No measurement with designation \"{0}\" found in sequence \"{1}\"",
                designation,
                SequenceDescriptionForExport.Id));
        }

        /// <summary>
        /// Gets or sets the sequence series metadata.
        /// </summary>
        /// <value>The sequence series metadata.</value>
        [JsonProperty ("metadata")]
        [YAXSerializableField]
        [YAXSerializeAs ("metadata")]
        public ISequenceSeriesMetadata Metadata {
            get;
            set;
        }

        /// <summary>
        /// Gets the sequence description.
        /// </summary>
        /// <value>The sequence description.</value>
        public ISequenceDescription SequenceDescription {
            get {
                return SequenceDescriptionForExport;
            }
        }

        /// <summary>
        /// Gets the seqeunce description for export.
        /// </summary>
        /// <value>The sequence description for export.</value>
        [JsonProperty ("sequenceDescription")]
        [YAXSerializableField]
        [YAXSerializeAs ("sequenceDescription")]
        public ISequenceDescription SequenceDescriptionForExport {
            get;
            set;
        }

        /// <summary>
        /// Gets the sequences.
        /// </summary>
        /// <value>The sequences.</value>
        [JsonProperty("sequences")]
        [YAXSerializableField]
        [YAXSerializeAs("sequences")]
        [YAXCollection(YAXCollectionSerializationTypes.Recursive, EachElementName = "sequence")]
        public List<ISequence> Sequences {
            get {
                return this.sequences;
            }
        }

        private void NotifyMeasurementAdded(MeasurementAlignmentIdentifier identifier, IMeasurement measurement)
        {
            if (MeasurementAdded != null) {
                MeasurementAdded(identifier, measurement);
            }
        }

        /// <summary>
        /// Gets or sets the measurement descriptions.
        /// </summary>
        /// <value>The measurement descriptions.</value>
        public Dictionary<string, IMeasurementDescription> MeasurementDescriptions {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the measurement descriptions for export.
        /// </summary>
        /// <value>The measurement descriptions for export.</value>
        [JsonProperty("measurementDescriptions")]
        [YAXSerializableField]
        [YAXSerializeAs("measurementDescriptions")]
        [YAXDictionary(
            EachPairName = "measurement",
            KeyName = "id",
            SerializeKeyAs = YAXNodeTypes.Attribute,
            ValueName = "measurementDescription",
            SerializeValueAs = YAXNodeTypes.Element)]
        public Dictionary<string, IMeasurementDescription> MeasurementDescriptionsForExport {
            get {
                Dictionary<string, IMeasurementDescription> result = new Dictionary<string, IMeasurementDescription>();
                foreach (string id in MeasurementDescriptions.Keys) {
                    result.Add (id, MeasurementDescriptions[id]);
                }
                return  result;
            }
            set {
                MeasurementDescriptions = value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has selected items.
        /// </summary>
        /// <value><c>true</c> if this instance has selected items; otherwise, <c>false</c>.</value>
        public bool HasSelectedItems {
            get {
                foreach (ISequence sequence in Sequences) {
                    foreach (IMeasurement measurement in sequence.Measurements.Values) {
                        if (measurement.Active) {
                            return true;
                        }
                    }
                }
                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has items.
        /// </summary>
        /// <value><c>true</c> if this instance has items; otherwise, <c>false</c>.</value>
        public bool HasItems {
            get {
                foreach (ISequence sequence in Sequences) {
                    if (sequence.Measurements.Count > 0) {
                        return true;
                    }
                }
                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has curve.
        /// </summary>
        /// <value><c>true</c> if this instance has curve; otherwise, <c>false</c>.</value>
        public bool HasCurve {
            get {
                foreach (IMeasurementDescription measurementDescription in MeasurementDescriptions.Values) {
                    if (measurementDescription.CurveDescription != null) {
                        return true;
                    }
                }
                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has attributes.
        /// </summary>
        /// <value><c>true</c> if this instance has attributes; otherwise, <c>false</c>.</value>
        public bool HasAttributes {
            get {
                return Metadata.TextualAttributes != null && Metadata.TextualAttributes.Count > 0;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has parameters.
        /// </summary>
        /// <value><c>true</c> if this instance has parameters; otherwise, <c>false</c>.</value>
        public bool HasParameters {
            get {
                foreach (IMeasurementMetadata measurement in Metadata.Measurements.Values) {
                    if (measurement.Parameters != null && measurement.Parameters.Count > 0) {
                        return true;
                    }
                }
                return false;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="ProbeNet.Export.SequenceContainer"/> export curve.
        /// </summary>
        /// <value><c>true</c> if export curve; otherwise, <c>false</c>.</value>
        public bool ExportCurve {
            get {
                return exportCurve;
            }
            set {
                if (!HasCurve) {
                    value = false;
                }
                exportCurve = value;
            }
        }
    }
}

