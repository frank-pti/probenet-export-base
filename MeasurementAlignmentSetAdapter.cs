using ProbeNet.Messages.Base;
using ProbeNet.Messages.Interface;
/*
 * The base library for ProbeNet exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;

namespace ProbeNet.Export
{
    /// <summary>
    /// Measurement alignment set adapter.
    /// </summary>
    public class MeasurementAlignmentSetAdapter
    {
        /// <summary>
        /// Delegate method for measurement added event.
        /// </summary>
        public delegate void MeasurementsAddedDelegate(IList<IMeasurement> measurements);
        /// <summary>
        /// Occurs when measurements added.
        /// </summary>
        public event MeasurementsAddedDelegate MeasurementsAdded;

        /// <summary>
        /// Delegate method for measurement active state changed event.
        /// </summary>
        public delegate void MeasurementActiveStateChangedDelegate(IMeasurement sender, int index);
        /// <summary>
        /// Occurs when measurement active state changed.
        /// </summary>
        public event MeasurementActiveStateChangedDelegate MeasurementActiveStateChanged;

        private DataSourceDataModelBase model;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.MeasurementAlignmentSetAdapter"/> class.
        /// </summary>
        /// <param name="model">Model.</param>
        /// <param name="identifier">Identifier.</param>
        public MeasurementAlignmentSetAdapter(DataSourceDataModelBase model, MeasurementAlignmentIdentifier identifier)
        {
            if (!model.Measurements.ContainsKey(identifier.MeasurementDescription.Id)) {
                throw new ArgumentException(String.Format("Model does not contain key '{0}'", identifier.MeasurementDescription.Id));
            }
            if (!model.Measurements[identifier.MeasurementDescription.Id].ContainsKey(identifier)) {
                throw new ArgumentException(String.Format("Model does not contain identifier '{0}'", identifier));
            }
            Identifier = identifier;
            Model = model;
        }

        private void HandleSequenceAdded(
            DataSourceDataModelBase sender, ISequenceDescription description, ISequence sequence)
        {
            if (MeasurementsAdded == null) {
                return;
            }
            IList<IMeasurement> measurements =
                Identifier.MeasurementDescription.ExtractMeasurements(description, sequence, Identifier);
            foreach (IMeasurement measurement in measurements) {
                (measurement as Measurement).ActiveStateChanged += HandleActiveStateChanged;
            }
            if (measurements.Count > 0) {
                MeasurementsAdded(measurements);
            }
        }

        /// <summary>
        /// Gets the data source date model.
        /// </summary>
        /// <value>The model.</value>
        public DataSourceDataModelBase Model
        {
            get
            {
                return model;
            }
            private set
            {
                model = value;
                model.SequenceAdded += HandleSequenceAdded;
                foreach (IMeasurement measurement in Measurements) {
                    (measurement as Measurement).ActiveStateChanged += HandleActiveStateChanged;
                }
            }
        }

        private void HandleActiveStateChanged(IMeasurement measurement)
        {
            if (MeasurementActiveStateChanged != null) {
                MeasurementActiveStateChanged(measurement, Measurements.IndexOf(measurement));
            }
        }

        /// <summary>
        /// Gets the measurement alignment identifier.
        /// </summary>
        /// <value>The identifier.</value>
        public MeasurementAlignmentIdentifier Identifier
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the measurements.
        /// </summary>
        /// <value>The measurements.</value>
        public IList<IMeasurement> Measurements
        {
            get
            {
                return Model.Measurements[Description.Id][Identifier];
            }
        }

        /// <summary>
        /// Gets the measurement description.
        /// </summary>
        /// <value>The description.</value>
        public IMeasurementDescription Description
        {
            get
            {
                return Identifier.MeasurementDescription;
            }
        }

        /// <summary>
        /// Gets the measurement metadata.
        /// </summary>
        /// <value>The metadata.</value>
        public IMeasurementMetadata Metadata
        {
            get
            {
                return Model.MeasurementMetadata[Description.Id];
            }
        }

        public void UpdateDescriptions()
        {
            Model.UpdateDescriptions();
            IMeasurementDescription measurementDescription =
                Model.MeasurementDescriptions[Identifier.MeasurementDescription.Id];
            if (measurementDescription is ProbeNet.Messages.Raw.MeasurementDescription) {
                Identifier = new MeasurementAlignmentIdentifier(
                    measurementDescription, Identifier.Side, Identifier.Direction);
            }
        }
    }
}

