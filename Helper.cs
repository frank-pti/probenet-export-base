/*
 * The base library for ProbeNet exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Internationalization;
using ProbeNet.Messages;
using ProbeNet.Messages.Interface;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace ProbeNet.Export
{
    /// <summary>
    /// Helper class with some usable methods.
    /// </summary>
    public static class Helper
    {
        /// <summary>
        /// Gets the spreadsheet formula from the corresponding attribute.
        /// </summary>
        /// <returns>The spreadsheet formula.</returns>
        /// <param name="method">Method.</param>
        public static string GetMethodFormula(this MethodInfo method)
        {
            return method.GetCustomAttributes<SpreadsheetFormulaAttribute>("Statistics method")[0].Formula;
        }

        /// <summary>
        /// Gets the custom attributes.
        /// </summary>
        /// <returns>The custom attributes.</returns>
        /// <param name="member">Member.</param>
        /// <param name="name">Name, used for error messages.</param>
        /// <typeparam name="TAttribute">The attribute type parameter.</typeparam>
        public static TAttribute[] GetCustomAttributes<TAttribute>(this MemberInfo member, string name = "Member ")
            where TAttribute : Attribute
        {
            return (TAttribute[])member.GetCustomAttributes(typeof(TAttribute), name);
        }

        /// <summary>
        /// Gets the custom attributes.
        /// </summary>
        /// <returns>The custom attributes.</returns>
        /// <param name="member">Member.</param>
        /// <param name="attributeType">Attribute type.</param>
        /// <param name="name">Name, used for error messages.</param>
        public static object[] GetCustomAttributes(this MemberInfo member, Type attributeType, string name = "Member ")
        {
            object[] attributes = member.GetCustomAttributes(attributeType, true);
            if (attributes.Length == 0 && name != null) {
                throw new ArgumentException(
                    String.Format("{0} '{1}' is missing {2}", name, member.Name, attributeType.Name));
            }
            return attributes;
        }

        /// <summary>
        /// Determines if the specified method has attributes.
        /// </summary>
        /// <returns><c>true</c> if the specified method has attribute; otherwise, <c>false</c>.</returns>
        /// <param name="method">Method.</param>
        /// <typeparam name="TAttribute">The attribute type parameter.</typeparam>
        public static bool HasAttribute<TAttribute>(this MethodInfo method)
            where TAttribute : Attribute
        {
            return method.GetCustomAttributes<TAttribute>(null).Length > 0;
        }

        /// <summary>
        /// Gets the color of the chart series at the specified index.
        /// </summary>
        /// <returns>The chart series color.</returns>
        /// <param name="index">Index.</param>
        public static Cairo.Color GetChartSeriesColor(int index)
        {
            return CairoChart.ColorList.GetColor(index);
        }

        /// <summary>
        /// Gets the chart series color as uint.
        /// </summary>
        /// <returns>The chart series color as uint.</returns>
        /// <param name="index">Index.</param>
        public static uint GetChartSeriesColorAsUint(int index)
        {
            Cairo.Color color = GetChartSeriesColor(index);
            uint red = (uint)(color.R * 255);
            uint green = (uint)(color.G * 255);
            uint blue = (uint)(color.B * 255);
            return (red << 16) | (green << 8) | blue;
        }

        /// <summary>
        /// Builds the parameter list.
        /// </summary>
        /// <returns>The parameter list.</returns>
        /// <param name="i18n">I18n.</param>
        /// <param name="metadata">Metadata.</param>
        /// <param name="description">Description.</param>
        public static ParameterList BuildParameterList(I18n i18n, IMeasurementMetadata metadata, IMeasurementDescription description)
        {
            IDictionary<TranslationString, IParameterDescription> parameters = ParameterHelper.ExtractParameters(i18n, description);

            ParameterList result = new ParameterList();
            int i = 0;
            foreach (KeyValuePair<TranslationString, IParameterDescription> parameter in parameters) {
                result.Add(
                    parameter.Key.Tr,
                    ParameterHelper.FormatParameter(i18n, metadata.Parameters[parameter.Value.Id], parameter.Value));
                i++;
            }
            return result;
        }
    }
}

