/*
 * The base library for ProbeNet exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace ProbeNet.Export
{
    /// <summary>
    /// Attribute for using the default precision.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class UseDefaultPrecisionAttribute: Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.UseDefaultPrecisionAttribute"/> class.
        /// </summary>
        public UseDefaultPrecisionAttribute ():
            this(null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.UseDefaultPrecisionAttribute"/> class.
        /// </summary>
        /// <param name="precision">Precision.</param>
        public UseDefaultPrecisionAttribute (Nullable<int> precision)
        {
            Precision = precision;
        }

        /// <summary>
        /// Gets the precision.
        /// </summary>
        /// <value>The precision.</value>
        public Nullable<int> Precision {
            get;
            private set;
        }
    }
}

