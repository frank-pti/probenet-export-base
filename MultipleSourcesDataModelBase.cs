using Newtonsoft.Json;
/*
 * The base library for ProbeNet exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.IO;
using YAXLib;

namespace ProbeNet.Export
{
    /// <summary>
    /// Multiple sources data model base.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("probenetSet")]
    public abstract class MultipleSourcesDataModelBase : ICustomParameters
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.MultipleSourcesDataModelBase"/> class.
        /// </summary>
        public MultipleSourcesDataModelBase()
        {
        }

        /// <summary>
        /// Gets the data models.
        /// </summary>
        /// <returns>The data models.</returns>
        public abstract IEnumerable<DataSourceDataModelBase> GetDataModels();

        /// <summary>
        /// Gets the data models (the measurement sets).
        /// </summary>
        /// <value>The data models.</value>
        [JsonProperty("measurementSets")]
        [YAXSerializableField]
        [YAXCollection(YAXCollectionSerializationTypes.RecursiveWithNoContainingElement)]
        public IList<MeasurementSetAdapter> DataModels
        {
            get
            {
                IList<DataSourceDataModelBase> allModels = new List<DataSourceDataModelBase>(GetDataModels());
                List<MeasurementSetAdapter> models = new List<MeasurementSetAdapter>();

                if (MeasurementIdsForExport == null) {
                    foreach (DataSourceDataModelBase model in allModels) {
                        model.MeasurementIdsForExport = null;
                        models.AddRange(model.MeasurementSets);
                    }
                    return models;
                }

                if (allModels.Count != MeasurementIdsForExport.Count) {
                    throw new Exception(String.Format(
                        "The number of MeasurementIdsForExport ({0}) is not the same as the number of all data models ({1})",
                        MeasurementIdsForExport.Count, allModels.Count));
                }


                int i = 0;

                foreach (DataSourceDataModelBase model in allModels) {
                    if (MeasurementIdsForExport[i] != null && MeasurementIdsForExport.Count > 0) {
                        model.MeasurementIdsForExport = MeasurementIdsForExport[i];
                        models.AddRange(model.MeasurementSets);
                    }
                    i++;
                }
                return models;
            }
        }

        /// <summary>
        /// Gets or sets the measurement identifiers for export.
        /// </summary>
        /// <value>The measurement identifiers for export.</value>
        [YAXDontSerialize]
        public IList<IList<string>> MeasurementIdsForExport
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the name of the custom parameters profile.
        /// </summary>
        /// <value>The custom parameters profile.</value>
        [JsonProperty("customParametersProfile")]
        [YAXSerializableField]
        [YAXSerializeAs("profile")]
        [YAXAttributeFor("customParameters")]
        public string CustomParametersProfileName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating wthether the custom parameters profile name should be printed.
        /// </summary>
        /// <value><c>true</c> if the custom parameters profile should be printed; otherwise, <c>false</c>.</value>
        [JsonProperty("printCustomParametersProfileName")]
        [YAXSerializableField]
        [YAXSerializeAs("printName")]
        [YAXAttributeFor("customParameters")]
        public bool PrintCustomParametersProfileName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the custom parameters.
        /// </summary>
        /// <value>The custom parameters.</value>
        [JsonIgnore]
        [YAXSerializableField]
        [YAXSerializeAs("customParameters")]
        [YAXCollection(YAXCollectionSerializationTypes.Recursive, EachElementName = "parameter")]
        public ParameterList CustomParameters
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the custom parameters for JSON.
        /// </summary>
        /// <value>The custom parameters for JSON.</value>
        [JsonProperty("customParameters")]
        [YAXDontSerialize]
        public IDictionary<string, string> CustomParametersForJson
        {
            get
            {
                if (CustomParameters == null) {
                    return null;
                }
                IDictionary<string, string> result = new Dictionary<string, string>();
                foreach (Parameter parameter in CustomParameters) {
                    result.Add(parameter.Name, parameter.Value);
                }
                return result;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has diagram.
        /// </summary>
        /// <value><c>true</c> if this instance has diagram; otherwise, <c>false</c>.</value>
        [YAXDontSerialize]
        public bool HasDiagram
        {
            get
            {
                foreach (DataSourceDataModelBase model in GetDataModels()) {
                    if (model.HasDiagram) {
                        return true;
                    }
                }
                return false;
            }
        }

        /// <summary>
        /// Serializes the stream json.
        /// </summary>
        /// <param name="writeStream">Write stream.</param>
        public void SerializeStreamJson(Stream writeStream)
        {
            TextWriter textWriter = new StreamWriter(writeStream);
            JsonTextWriter jsonWriter = new JsonTextWriter(textWriter);
            jsonWriter.Formatting = Formatting.Indented;

            try {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(jsonWriter, this);
                textWriter.Flush();
            } catch (Exception e) {
                throw e;
            }
        }

        /// <summary>
        /// Deserilaize from stram json
        /// </summary>
        /// <param name="readStream">Read stream.</param>
        /// <returns>The object read.</returns>
        public static Newtonsoft.Json.Linq.JObject DeserialzeStreamJson(Stream readStream)
        {
            Newtonsoft.Json.Linq.JObject content = null;
            TextReader textReader = new StreamReader(readStream);
            JsonTextReader jsonReader = new JsonTextReader(textReader);
            try {
                JsonSerializer serializer = new JsonSerializer();
                content = (Newtonsoft.Json.Linq.JObject)serializer.Deserialize(jsonReader);
            } catch (Exception e) {
                throw e;
            }
            return content;
        }
    }
}

