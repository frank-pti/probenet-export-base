/*
 * The base library for ProbeNet exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Internationalization;
using Logging;
using ProbeNet.Messages.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Text;
using System.Threading;
using TranslatableInterface = ProbeNet.Messages.Translatable.Interface;

namespace ProbeNet.Export
{
    /// <summary>
    /// Container class for statistics values.
    /// </summary>
    public class StatisticsContainer : IContainer
    {
        private I18n i18n;
        private Nullable<double>[,] values;
        private int[,] precisions;
        private IDictionary<string, NumericValueDisplayFormat> displayFormats;
        private IDictionary<string, TranslationString> columnNames;
        private IList<string> columnIds;
        private Thread calculationThread;

        /// <summary>
        /// Delegate method for value changed event.
        /// </summary>
        public delegate void ValueChangedDelegate(int x, int y, Nullable<double> value);
        /// <summary>
        /// Occurs when value changed.
        /// </summary>
        public event ValueChangedDelegate ValueChanged;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.StatisticsContainer"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="model">Model.</param>
        /// <param name="displayFormats">Display formats.</param>
        public StatisticsContainer(
            I18n i18n, MeasurementAlignmentSetAdapter model, IDictionary<string, NumericValueDisplayFormat> displayFormats)
        {
            if (model.Measurements.Count == 0) {
                throw new ArgumentException("The SequenceContainerMeasurementView must contain at least one result.", "container");
            }
            this.i18n = i18n;
            Model = model;
            this.displayFormats = displayFormats;

            columnNames = BuildColumnNames();
            columnIds = BuildColumnIds();
            Methods = Statistics.GetMethods();
            IMeasurement first = Model.Measurements[0];
            values = new Nullable<double>[first.Results.Count, Methods.Count];
            precisions = new int[first.Results.Count, Methods.Count];

            Model.MeasurementsAdded += HandleMeasurementsAdded;
            Model.MeasurementActiveStateChanged += HandleMeasurementActiveStateChanged;

            StartCalculationThread();
        }

        /// <summary>
        /// Waits for calculation to finish.
        /// </summary>
        public void WaitForCalculationToFinish()
        {
            if (calculationThread == null || !calculationThread.IsAlive) {
                return;
            }
            calculationThread.Join();
        }

        private void HandleMeasurementActiveStateChanged(IMeasurement sender, int index)
        {
            StartCalculationThread();
        }

        private void HandleMeasurementsAdded(IList<IMeasurement> measurements)
        {
            StartCalculationThread();
        }

        /// <summary>
        /// Starts the calculation thread.
        /// </summary>
        public void StartCalculationThread()
        {
            if (calculationThread != null && calculationThread.IsAlive) {
                calculationThread.Interrupt();
            }
            calculationThread = new Thread(UpdateStatisticsInThread);
            calculationThread.Name = "Statistics calculation";
            calculationThread.Priority = ThreadPriority.Lowest;
            calculationThread.IsBackground = true;
            calculationThread.Start();
        }

        private void UpdateStatisticsInThread()
        {
            try {
                for (int i = 0; i < Methods.Count; i++) {
                    CalculateStatistics(i);
                }
            } catch (ThreadInterruptedException) {
                Logger.Log("Calculation thread was interrupted, probably a new one was started.");
            } catch (Exception e) {
                Logger.Log(e, "Error while updating statistics");
                throw e;
            }
        }

        private IList<string> BuildColumnIds()
        {
            IList<string> ids = new List<string>(Model.Description.ResultDescriptions.Count);
            foreach (IResultDescription resultDescription in Model.Description.ResultDescriptions) {
                ids.Add(resultDescription.Id);
            }
            return ids;
        }

        private IDictionary<string, TranslationString> BuildColumnNames()
        {
            IDictionary<string, TranslationString> names = new Dictionary<string, TranslationString>();
            foreach (IResultDescription description in Model.Description.ResultDescriptions) {
                TranslationString caption = null;
                TranslationString unit = null;
                if (description is TranslatableInterface.ITranslatableValueDescription) {
                    TranslatableInterface.ITranslatableValueDescription translatableDescription =
                        description as TranslatableInterface.ITranslatableValueDescription;
                    caption = translatableDescription.Caption;
                    unit = translatableDescription.Unit;
                } else {
                    caption = i18n.TrObject(Constants.UiDomain, "{0}", description.Caption);
                    unit = i18n.TrObject(Constants.UiDomain, "{0}", description.Unit);
                }

                if (String.IsNullOrWhiteSpace(unit.Tr)) {
                    names.Add(description.Id, caption);
                } else {
                    names.Add(description.Id, i18n.TrObject(Constants.UiDomain, "{0} [{1}]", caption, unit));
                }
            }
            return names;
        }

        private void CalculateStatistics(int methodIndex)
        {
            for (int i = 0; i < Model.Description.ResultDescriptions.Count; i++) {
                try {
                    IResultDescription resultDescription = Model.Description.ResultDescriptions[i];
                    MethodInfo method = Methods[methodIndex];
                    int precision = CalculatePrecision(method, resultDescription);
                    values[i, methodIndex] = CalculateStatistics(method, resultDescription.Id, ref precision);
                    precisions[i, methodIndex] = precision;
                    NotifyValueChanged(i, methodIndex);
                } catch (Exception e) {
                    Logger.Log(e);
                }
            }
        }

        private int CalculatePrecision(MethodBase member, IResultDescription resultDescription)
        {
            int precision = resultDescription != null ? resultDescription.DecimalPlaces : Statistics.DefaultPrecision;
            UseDefaultPrecisionAttribute[] useDefaultPrecisionAttributes =
                member.GetCustomAttributes(typeof(UseDefaultPrecisionAttribute), false) as UseDefaultPrecisionAttribute[];
            if (useDefaultPrecisionAttributes != null && useDefaultPrecisionAttributes.Length > 0) {
                Nullable<int> useDefaultPrecisionValue = useDefaultPrecisionAttributes[0].Precision;
                if (useDefaultPrecisionValue != null && useDefaultPrecisionValue.HasValue) {
                    precision = useDefaultPrecisionValue.Value;
                } else {
                    // UseDefaultPrecisionAttribute exists, but no value set
                    if (PrecisionFromSettings != null && PrecisionFromSettings.HasValue) {
                        precision = PrecisionFromSettings.Value;
                    } else {
                        precision = Statistics.DefaultPrecision;
                    }
                }
            }
            return precision;
        }

        private Nullable<double> CalculateStatistics(MethodBase method, string columnId, ref int precision)
        {
            int valuesIndex = 0;
            IList<IMeasurement> measurements = new List<IMeasurement>(Model.Measurements);
            int selectedItems = Model.Measurements.GetSelectedItems().Values.Count;
            if (selectedItems == 0) {
                return double.NaN;
            }
            Nullable<double>[] inputValues = new Nullable<double>[selectedItems];
            foreach (IMeasurement measurement in measurements) {
                if (measurement.Active) {
                    try {
                        inputValues[valuesIndex] = measurement.Results[columnId];
                        valuesIndex++;
                    } catch (KeyNotFoundException knfe) {
                        Logger.Error("Key '{0}' not found in {{{1}}}.", columnId, String.Join(",", measurement.Results.Keys));
                        Logger.Log(knfe);
                    } catch (Exception e) {
                        Logger.Log(e);
                    }
                }
            }
            object[] inputParams = new object[] { precision, inputValues };
            Nullable<double> result = (Nullable<double>)method.Invoke(null, inputParams);
            precision = (int)inputParams[0];
            return result;
        }

        private void NotifyValueChanged(int x, int y)
        {
            if (ValueChanged != null) {
                ValueChanged(x, y, values[x, y]);
            }
        }

        /// <summary>
        /// Gets the value at the specified location.
        /// </summary>
        /// <returns>The value.</returns>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        public Nullable<double> GetValue(int x, int y)
        {
            return values[x, y];
        }

        /// <summary>
        /// Gets the precision at the specified location.
        /// </summary>
        /// <returns>The precision.</returns>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        public int GetPrecision(int x, int y)
        {
            return precisions[x, y];
        }

        /// <summary>
        /// Gets the type of the numeric value display format for the specified method.
        /// </summary>
        /// <returns>The format type.</returns>
        /// <param name="methodDescription">Method description.</param>
        public NumericValueDisplayFormat GetFormatType(string methodDescription)
        {
            return displayFormats[methodDescription];
        }

        /// <summary>
        /// Gets the type of the numeric value display format for the specified method.
        /// </summary>
        /// <returns>The format type.</returns>
        /// <param name="methodIndex">Method index.</param>
        public NumericValueDisplayFormat GetFormatType(int methodIndex)
        {
            return displayFormats[GetMethodName(methodIndex)];
        }

        /// <summary>
        /// Gets the name of the method at the specified index.
        /// </summary>
        /// <returns>The method name.</returns>
        /// <param name="methodIndex">Method index.</param>
        public string GetMethodName(int methodIndex)
        {
            MethodInfo method = Methods[methodIndex];
            DescriptionAttribute[] descriptionAttributes =
                method.GetCustomAttributes(typeof(DescriptionAttribute), false) as DescriptionAttribute[];
            if (descriptionAttributes == null || descriptionAttributes.Length == 0) {
                throw new ArgumentException(String.Format("The method {0} is missing the Description attribute",
                                                           method.Name));
            }
            return descriptionAttributes[0].Description;
        }

        /// <summary>
        /// Gets the number format at the specified location.
        /// </summary>
        /// <returns>The number format.</returns>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        public string GetNumberFormat(int x, int y)
        {
            return GetNumberFormat(GetFormatType(GetMethodName(y)), GetPrecision(x, y));
        }

        /// <summary>
        /// Gets the number format for the speicified numeric value display format and precision.
        /// </summary>
        /// <returns>The number format.</returns>
        /// <param name="format">Format.</param>
        /// <param name="precision">Precision.</param>
        public static string GetNumberFormat(NumericValueDisplayFormat format, int precision)
        {
            if (format == NumericValueDisplayFormat.Percent) {
                return String.Format("{{0:P{0}}}", precision - 2);
            }
            StringBuilder builder = new StringBuilder("{0:0.");
            for (int i = 0; i < precision; i++) {
                builder.Append("#");
            }
            builder.Append("}");
            return builder.ToString();
        }

        #region IContainer implementation
        /// <inheritdoc/>
        public int Rows
        {
            get
            {
                return values.GetLength(1);
            }
        }

        /// <inheritdoc/>
        public int ActiveRows
        {
            get
            {
                return Rows;
            }
        }

        /// <inheritdoc/>
        public int Columns
        {
            get
            {
                return values.GetLength(0);
            }
        }
        #endregion

        /// <summary>
        /// Gets the translatable name of the column with the specified id.
        /// </summary>
        /// <returns>The column name.</returns>
        /// <param name="id">Identifier.</param>
        public TranslationString GetColumnName(string id)
        {
            return columnNames[id];
        }

        /// <summary>
        /// Gets the translatable name of the column with the specified column number.
        /// </summary>
        /// <returns>The column name.</returns>
        /// <param name="columnNumber">Column number.</param>
        public TranslationString GetColumnName(int columnNumber)
        {
            return GetColumnName(GetColumnId(columnNumber));
        }

        /// <summary>
        /// Gets the column identifier of the column with the specified number.
        /// </summary>
        /// <returns>The column identifier.</returns>
        /// <param name="columnNumber">Column number.</param>
        public string GetColumnId(int columnNumber)
        {
            return columnIds[columnNumber];
        }

        /// <summary>
        /// Gets the statistics methods list.
        /// </summary>
        /// <value>The methods.</value>
        public IList<MethodInfo> Methods
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the model.
        /// </summary>
        /// <value>The model.</value>
        public MeasurementAlignmentSetAdapter Model
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the precision from settings.
        /// </summary>
        /// <value>The precision from settings.</value>
        public Nullable<int> PrecisionFromSettings
        {
            get;
            set;
        }

        public void UpdateDescriptions()
        {
            columnNames = BuildColumnNames();
        }
    }
}

