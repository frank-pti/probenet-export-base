/*
 * The base library for ProbeNet exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace ProbeNet.Export
{
    /// <summary>
    /// Attribute for attaching color information.
    /// </summary>
	public class ColorAttribute: Attribute
	{
		private Gdk.Color color;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.ColorAttribute"/> class.
        /// </summary>
        /// <param name="r">The red component.</param>
        /// <param name="g">The green component.</param>
        /// <param name="b">The blue component.</param>
		public ColorAttribute (byte r, byte g, byte b)
		{
			color = new Gdk.Color(r, g, b);
		}

        /// <summary>
        /// Gets the color.
        /// </summary>
        /// <value>The color.</value>
		public Gdk.Color Color {
			get {
				return color;
			}
		}

        /// <summary>
        /// Gets the rgb value
        /// </summary>
        /// <value>The rgb value.</value>
        public uint Rgb {
            get {
                return (uint)(color.Red << 8) | (uint)(color.Green << 4) | (uint)color.Blue;
            }
        }
	}
}

