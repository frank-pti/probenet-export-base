using Newtonsoft.Json;
/*
 * The base library for ProbeNet exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using YAXLib;

namespace ProbeNet.Export
{
    /// <summary>
    /// Parameter list.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("parameterList")]
    public class ParameterList : List<Parameter>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.ParameterList"/> class.
        /// </summary>
        public ParameterList()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.ParameterList"/> class.
        /// </summary>
        /// <param name="source">Source.</param>
        public ParameterList(IDictionary<string, string> source)
        {
            if (source == null) {
                return;
            }
            foreach (KeyValuePair<string, string> item in source) {
                Add(new Parameter(item));
            }
        }

        /// <summary>
        /// Add the specified name and value.
        /// </summary>
        /// <param name="name">Name.</param>
        /// <param name="value">Value.</param>
        public void Add(string name, string value)
        {
            Add(new Parameter(name, value));
        }

        /// <summary>
        /// Convert the parameter list to a two-dimensional array.
        /// </summary>
        /// <returns>The array.</returns>
        /// <param name="name">Name of the parameter list. It will be inserted into the first column in the first line.
        /// Pass <c>null</c> for omitting the name.</param>
        /// <param name="includeEmptyValues">If set to <c>true</c> include empty values.</param>
        public string[,] ToArray(string name, bool includeEmptyValues)
        {
            IList<Parameter> list = this;
            if (!includeEmptyValues) {
                list = this.FindAll((p) => !String.IsNullOrWhiteSpace(p.Value));
            }
            int count = list.Count;
            if (!String.IsNullOrWhiteSpace(name)) {
                count++;
            }
            string[,] result = new string[2, count];
            int row = 0;
            if (!String.IsNullOrWhiteSpace(name)) {
                result[0, row] = name;
                row++;
            }
            foreach (Parameter parameter in list) {
                result[0, row] = parameter.Name;
                result[1, row] = parameter.Value;
                row++;
            }
            return result;
        }
    }
}

