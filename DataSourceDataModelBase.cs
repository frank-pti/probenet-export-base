/*
 * The base library for ProbeNet exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using ProbeNet.Messages.Interface;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProbeNet.Export
{
    /// <summary>
    /// Data source data model base.
    /// </summary>
    public abstract class DataSourceDataModelBase
    {
        /// <summary>Delegate method for sequence added event.</summary>
        public delegate void SequenceAddedDelegate(
            DataSourceDataModelBase sender, ISequenceDescription description, ISequence sequence);
        /// <summary>
        /// Occurs when sequence added.
        /// </summary>
        public event SequenceAddedDelegate SequenceAdded;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.DataSourceDataModelBase"/> class.
        /// </summary>
        public DataSourceDataModelBase()
        {
            SequenceDescriptions = new Dictionary<string, ISequenceDescription>();
            MeasurementMetadata = new Dictionary<string, IMeasurementMetadata>();
            Measurements = new Dictionary<string, IDictionary<MeasurementAlignmentIdentifier, IList<IMeasurement>>>();
            MeasurementDescriptions = new Dictionary<string, IMeasurementDescription>();
        }

        /// <summary>
        /// Gets the measurement sets.
        /// </summary>
        /// <value>The measurement sets.</value>
        public IList<MeasurementSetAdapter> MeasurementSets
        {
            get
            {
                IList<MeasurementSetAdapter> measurementSets = new List<MeasurementSetAdapter>();
                IList<string> measurementIdsForExport = MeasurementIdsForExport;
                if (measurementIdsForExport == null) {
                    measurementIdsForExport = new List<string>(Measurements.Keys);
                }
                foreach (string measurementId in measurementIdsForExport) {
                    measurementSets.Add(new MeasurementSetAdapter(this, MeasurementDescriptions[measurementId]));
                }
                return measurementSets;
            }
        }

        /// <summary>
        /// Gets or sets the measurement identifiers for export.
        /// </summary>
        /// <value>The measurement identifiers for export.</value>
        public IList<string> MeasurementIdsForExport
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the visible result columns.
        /// </summary>
        /// <value>The visible result columns.</value>
        public virtual IDictionary<string, IList<string>> VisibleResultColumns
        {
            get
            {
                return BuildDefaultVisibleResultColumns();
            }
        }

        /// <summary>
        /// Gets the measurement metadata.
        /// </summary>
        /// <value>The measurement metadata.</value>
        public Dictionary<string, IMeasurementMetadata> MeasurementMetadata
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the sequence descriptions.
        /// </summary>
        /// <value>The sequence descriptions.</value>
        public Dictionary<string, ISequenceDescription> SequenceDescriptions
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the measurements.
        /// </summary>
        /// <value>The measurements.</value>
        public Dictionary<string, IDictionary<MeasurementAlignmentIdentifier, IList<IMeasurement>>> Measurements
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the measurement descriptions.
        /// </summary>
        /// <value>The measurement descriptions.</value>
        public Dictionary<string, IMeasurementDescription> MeasurementDescriptions
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the textual attributes.
        /// </summary>
        /// <value>The textual attributes.</value>
        public ParameterList TextualAttributes
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the custom measurement parameters.
        /// </summary>
        /// <value>The custom measurement parameters.</value>
        public IDictionary<string, ParameterList> CustomMeasurementParameters
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the current sequence description.
        /// </summary>
        /// <value>The current sequence description.</value>
        private ISequenceDescription CurrentSequenceDescription
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the current metadata.
        /// </summary>
        /// <value>The current metadata.</value>
        private ISequenceSeriesMetadata CurrentMetadata
        {
            get;
            set;
        }

        /// <summary>
        /// Gets a value indicating whether this instance has diagram.
        /// </summary>l
        /// <value><c>true</c> if this instance has diagram; otherwise, <c>false</c>.</value>
        public bool HasDiagram
        {
            get
            {
                foreach (string id in Measurements.Keys) {
                    IMeasurementDescription description = MeasurementDescriptions[id];
                    if (description.CurveDescription != null) {
                        IDictionary<MeasurementAlignmentIdentifier, IList<IMeasurement>> measurements = Measurements[id];
                        foreach (IList<IMeasurement> measurementList in measurements.Values) {
                            if (measurementList.Count > 0) {
                                return true;
                            }
                        }
                    }
                }
                return false;
            }
        }

        /// <summary>
        /// Determines whether this instance has data for save for the measurements specified by the measurementDescription.
        /// </summary>
        /// <returns><c>true</c> if this instance has data for save; otherwise, <c>false</c>.</returns>
        /// <param name="measurementDescription">Measurement description.</param>
        public bool HasDataForSave(IMeasurementDescription measurementDescription)
        {
            if (!Measurements.ContainsKey(measurementDescription.Id)) {
                return false;
            }
            return Measurements[measurementDescription.Id].Values.Count(
                measurements => measurements.Count(measurement => measurement.Active) > 0
            ) > 0;
        }

        /// <summary>
        /// Determines whether this instance has items the specified measurementDescription.
        /// </summary>
        /// <returns><c>true</c> if this instance has items the specified measurementDescription; otherwise, <c>false</c>.</returns>
        /// <param name="measurementDescription">Measurement description.</param>
        public bool HasItems(IMeasurementDescription measurementDescription)
        {
            if (!Measurements.ContainsKey(measurementDescription.Id)) {
                return false;
            }
            return Measurements[measurementDescription.Id].Values.Count<IList<IMeasurement>>(
                measurements => measurements.Count > 0) > 0;
        }

        /// <summary>
        /// Handles the sequence series metadata received.
        /// </summary>
        /// <param name="metadata">Metadata.</param>
        protected virtual void HandleSequenceSeriesMetadataReceived(ISequenceSeriesMetadata metadata)
        {
            string sequenceId = metadata.Sequence;
            if (!SequenceDescriptions.ContainsKey(sequenceId)) {
                KeyNotFoundException original = new KeyNotFoundException();
                throw new KeyNotFoundException(
                    String.Format("{0} Missing '{1}' in '{2}'", original.Message, sequenceId, String.Join("', '", SequenceDescriptions.Keys)));
            }
            CurrentSequenceDescription = SequenceDescriptions[sequenceId];
            if (CurrentMetadata != null) {
                if (!metadata.Uuid.Equals(CurrentMetadata.Uuid)) {
                    Logging.Logger.Log("New series started on device: {0}|{1}|{2}", this.DeviceUuid, this.DeviceModelNumber, this.DeviceSerialNumber);
                }
            }
            CurrentMetadata = metadata;
            TextualAttributes = new ParameterList(metadata.TextualAttributes);
            foreach (string measurementId in metadata.Measurements.Keys) {
                if (!MeasurementMetadata.ContainsKey(measurementId)) {
                    MeasurementMetadata.Add(measurementId, metadata.Measurements[measurementId]);
                }
            }
        }

        /// <summary>
        /// Handles the sequence received.
        /// </summary>
        /// <param name="sequence">Sequence.</param>
        protected void HandleSequenceReceived(ISequence sequence)
        {
            foreach (ISequenceDescriptionMeasurementReference reference in CurrentSequenceDescription.Measurements) {
                if (sequence.Measurements.ContainsKey(reference.Designation)) {
                    string measurementId = reference.Measurement.Id;
                    if (!Measurements.ContainsKey(measurementId)) {
                        Measurements.Add(measurementId, new Dictionary<MeasurementAlignmentIdentifier, IList<IMeasurement>>());
                        if (MeasurementDescriptions.ContainsKey(measurementId)) {
                            MeasurementDescriptions[measurementId] = reference.Measurement;
                        } else {
                            MeasurementDescriptions.Add(measurementId, reference.Measurement);
                        }
                    }
                    IMeasurement measurement = sequence.Measurements[reference.Designation];
                    MeasurementAlignmentIdentifier alignmentIdentifier = new MeasurementAlignmentIdentifier(
                        reference.Measurement, measurement.Alignment);
                    if (!Measurements[measurementId].ContainsKey(alignmentIdentifier)) {
                        Measurements[measurementId].Add(alignmentIdentifier, new List<IMeasurement>());
                    }
                    Measurements[measurementId][alignmentIdentifier].Add(measurement);
                }
            }
            NotifySequenceAdded(sequence);
        }

        private void NotifySequenceAdded(ISequence sequence)
        {
            if (SequenceAdded != null) {
                SequenceAdded(this, CurrentSequenceDescription, sequence);
            }
        }

        private IDictionary<string, IList<string>> BuildDefaultVisibleResultColumns()
        {
            IDictionary<string, IList<string>> visibleColumns = new Dictionary<string, IList<string>>();
            foreach (IMeasurementDescription description in MeasurementDescriptions.Values) {
                IList<string> results = new List<string>();
                foreach (IResultDescription resultDescription in description.ResultDescriptions) {
                    results.Add(resultDescription.Id);
                }
                visibleColumns.Add(description.Id, results);
            }
            return visibleColumns;
        }

        internal IDictionary<string, string> BuildCustomMeasurementParameters(string measurementId)
        {
            IDictionary<string, string> result = new Dictionary<string, string>();
            if (CustomMeasurementParameters == null) {
                return result;
            }
            if (!CustomMeasurementParameters.ContainsKey(measurementId)) {
                return result;
            }
            ParameterList parameterList = CustomMeasurementParameters[measurementId];
            foreach (Parameter parameter in parameterList) {
                if (!result.ContainsKey(parameter.Name)) {
                    result.Add(parameter.Name, parameter.Value);
                }
            }
            return result;
        }

        /// <summary>
        /// Clear this instance.
        /// </summary>
        public void Clear()
        {
            MeasurementMetadata.Clear();
            Measurements.Clear();
            MeasurementDescriptions.Clear();
        }

        /// <summary>
        /// Gets the device manufacturer.
        /// </summary>
        /// <value>The device manufacturer.</value>
        public abstract string DeviceManufacturer
        {
            get;
        }

        /// <summary>
        /// Gets the device model number.
        /// </summary>
        /// <value>The device model number.</value>
        public abstract string DeviceModelNumber
        {
            get;
        }

        /// <summary>
        /// Gets the device serial number.
        /// </summary>
        /// <value>The device serial number.</value>
        public abstract string DeviceSerialNumber
        {
            get;
        }


        protected abstract Guid DeviceUuid
        {
            get;
        }

        public abstract void UpdateDescriptions();
    }
}

