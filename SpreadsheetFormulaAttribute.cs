/*
 * The base library for ProbeNet exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace ProbeNet.Export
{
    /// <summary>
    /// Attribute defining the spreadsheet formula of an element.
    /// </summary>
	public class SpreadsheetFormulaAttribute: Attribute
	{
		private string formula;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.SpreadsheetFormulaAttribute"/> class.
        /// </summary>
        /// <param name="formula">Formula.</param>
		public SpreadsheetFormulaAttribute(string formula)
		{
			this.formula = formula;
		}

        /// <summary>
        /// Gets the formula.
        /// </summary>
        /// <value>The formula.</value>
		public string Formula {
			get {
				return formula;
			}
		}
	}
}

