/*
 * The base library for ProbeNet exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Reflection;
using Internationalization;

namespace ProbeNet.Export
{
    /// <summary>
    /// Exstension methods for MethodInfo class.
    /// </summary>
    public static class MethodInfoExtension
    {
        /// <summary>
        /// Gets the translated name of the method.
        /// </summary>
        /// <returns>The translated method name.</returns>
        /// <param name="method">Method.</param>
        /// <param name="i18n">I18n.</param>
        public static string GetTranslatedMethodName(this MethodInfo method, I18n i18n)
        {
            string translatedName = method.Name;
            object[] attributes = method.GetCustomAttributes(typeof(TranslatableCaptionAttribute), true);
            if (attributes.Length > 0) {
                TranslatableCaptionAttribute attribute = attributes[0] as TranslatableCaptionAttribute;
                translatedName = attribute.GetTranslation(i18n).Tr;
            }
            return translatedName;
        }

        /// <summary>
        /// Gets the translated short name of the method.
        /// </summary>
        /// <returns>The translated method short name.</returns>
        /// <param name="method">Method.</param>
        /// <param name="i18n">I18n.</param>
        public static string GetTranslatedMethodShortName(this MethodInfo method, I18n i18n)
        {
            string translatedName = method.Name;
            object[] attributes = method.GetCustomAttributes(typeof(TranslatableCaptionAttribute), true);
            if (attributes.Length > 0) {
                TranslatableCaptionAttribute attribute = attributes[0] as TranslatableCaptionAttribute;
                translatedName = attribute.GetShortTranslationIfAvailable(i18n).Tr;
            }
            return translatedName;
        }
    }
}

