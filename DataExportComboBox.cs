using Frank.Widgets;
using Internationalization;
/*
 * The base library for ProbeNet exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System.Collections.Generic;

namespace ProbeNet.Export
{
    /// <summary>
    /// ComoboBox that displays a list of data exports.
    /// </summary>
    public class DataExportComboBox : TranslatableListComboBox<IExportInformation>
    {
        public DataExportComboBox(I18n i18n, IList<IExportInformation> exportList)
            : base(i18n, exportList)
        {
        }

        /// <summary>
        /// Builds the data string (the string that is displayed to the user).
        /// </summary>
        /// <returns>The data string.</returns>
        /// <param name="dataExport">Data export.</param>
        protected override string BuildDataString(IExportInformation dataExport, object[] renderParameters)
        {
            return dataExport.GetCaption((I18n)renderParameters[0]).Tr;
        }
    }
}

