/*
 * The base library for ProbeNet exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using ProbeNet.Messages.Interface;
using System.Collections.Generic;

namespace ProbeNet.Export
{
    /// <summary>
    /// Extension methods for IMeasurement class.
    /// </summary>
    public static class IMeasurementExtensions
    {
        /// <summary>
        /// Gets the selected items.
        /// </summary>
        /// <returns>The selected items.</returns>
        /// <param name="measurements">Measurements.</param>
        public static IDictionary<int, IMeasurement> GetSelectedItems(this IEnumerable<IMeasurement> measurements)
        {
            return measurements.GetItems(true);
        }

        /// <summary>
        /// Gets the inactive items.
        /// </summary>
        /// <returns>The inactive items.</returns>
        /// <param name="measurements">Measurements.</param>
        public static IDictionary<int, IMeasurement> GetInactiveItems(this IEnumerable<IMeasurement> measurements)
        {
            return measurements.GetItems(false);
        }

        /// <summary>
        /// Gets all items (both active and inactive).
        /// </summary>
        /// <returns>The all items.</returns>
        /// <param name="measurements">Measurements.</param>
        public static IDictionary<int, IMeasurement> GetAllItems(this IEnumerable<IMeasurement> measurements)
        {
            return measurements.GetItems(null);
        }

        private static IDictionary<int, IMeasurement> GetItems(
            this IEnumerable<IMeasurement> measurements, Nullable<bool> value)
        {
            Dictionary<int, IMeasurement> result = new Dictionary<int, IMeasurement>();
            int measurementNumber = 0;
            foreach (IMeasurement measurement in new List<IMeasurement>(measurements)) {
                measurementNumber++;
                if (value == null || measurement.Active == value.Value) {
                    result.Add(measurementNumber, measurement);
                }
            }
            return result;
        }

        /// <summary>
        /// Extracts the measurements.
        /// </summary>
        /// <returns>The measurements.</returns>
        /// <param name="measurementDescription">Measurement description.</param>
        /// <param name="description">Description.</param>
        /// <param name="sequence">Sequence.</param>
        /// <param name="identifier">Identifier.</param>
        public static IList<IMeasurement> ExtractMeasurements(
            this IMeasurementDescription measurementDescription,
            ISequenceDescription description, ISequence sequence, MeasurementAlignmentIdentifier identifier)
        {
            IList<IMeasurement> measurements = new List<IMeasurement>();
            foreach (ISequenceDescriptionMeasurementReference reference in description.Measurements) {
                if (reference.Measurement.Id == measurementDescription.Id) {
                    IMeasurement measurement = sequence.Measurements[reference.Designation];
                    MeasurementAlignmentIdentifier comparisonAlignmentIdentifier =
                        new MeasurementAlignmentIdentifier(measurementDescription, measurement.Alignment);
                    if (identifier == null || identifier == comparisonAlignmentIdentifier) {
                        measurements.Add(measurement);
                    }
                }
            }
            return measurements;
        }
    }
}

