/*
 * The base library for ProbeNet exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using YAXLib;

namespace ProbeNet.Export
{
    /// <summary>
    /// List of visible result items.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("visibleResultList")]
    public class VisibleResultList
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.VisibleResultList"/> class.
        /// </summary>
        [JsonConstructor]
        public VisibleResultList()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.VisibleResultList"/> class.
        /// </summary>
        /// <param name="measurementId">Measurement identifier.</param>
        /// <param name="results">Results.</param>
        public VisibleResultList(string measurementId, IList<string> results)
        {
            MeasurementId = measurementId;
            Results = results;
        }

        /// <summary>
        /// Gets or sets the measurement identifier.
        /// </summary>
        /// <value>The measurement identifier.</value>
        [JsonProperty("measurement")]
        [YAXSerializableField]
        [YAXAttributeForClass]
        [YAXSerializeAs("id")]
        public string MeasurementId {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the results.
        /// </summary>
        /// <value>The results.</value>
        [JsonProperty("results")]
        [YAXDontSerialize]
        public IList<string> Results {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the results as object that can be processed by the YAXlib for serializing as XML.
        /// </summary>
        /// <value>The results.</value>
        [JsonIgnore]
        [YAXSerializableField]
        [YAXCollection(YAXCollectionSerializationTypes.RecursiveWithNoContainingElement, EachElementName = "result")]
        public IList<VisibleResultItem> ResultsXml {
            get {
                if (Results == null) {
                    return null;
                }
                IList<VisibleResultItem> items = new List<VisibleResultItem>();
                foreach (string result in Results) {
                    items.Add(new VisibleResultItem(result));
                }
                return items;
            }
            set {
                if (value == null) {
                    Results = null;
                    return;
                }
                Results.Clear();
                foreach (VisibleResultItem item in value) {
                    Results.Add(item.Item);
                }
            }
        }
    }
}

