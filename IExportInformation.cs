/*
 * The base library for ProbeNet exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Internationalization;

namespace ProbeNet.Export
{
    /// <summary>
    /// Interface for export information.
    /// </summary>
    public interface IExportInformation
    {
        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <value>The type.</value>
        Type Type {
            get;
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>The name.</value>
        string Name {
            get;
        }

        /// <summary>
        /// Gets the caption.
        /// </summary>
        /// <returns>The caption.</returns>
        /// <param name="i18n">I18n.</param>
        TranslationString GetCaption(I18n i18n);

        /// <summary>
        /// Creates the export.
        /// </summary>
        /// <returns>The export.</returns>
        /// <param name="settingsProvider">Settings provider.</param>
        DataExport CreateExport(SettingsProvider settingsProvider);

        /// <summary>
        /// Creates the export.
        /// </summary>
        /// <returns>The export.</returns>
        /// <param name="settingsProvider">Settings provider.</param>
        /// <typeparam name="T">The export type parameter.</typeparam>
        T CreateExport<T>(SettingsProvider settingsProvider)
            where T: DataExport;
    }
}

