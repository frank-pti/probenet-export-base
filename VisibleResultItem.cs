/*
 * The base library for ProbeNet exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;

namespace ProbeNet.Export
{
    /// <summary>
    /// Class for visible result items.
    /// </summary>
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("visibleResultItem")]
	public class VisibleResultItem
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.VisibleResultItem"/> class.
        /// </summary>
        public VisibleResultItem()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.VisibleResultItem"/> class.
        /// </summary>
        /// <param name="item">Item.</param>
        public VisibleResultItem(string item)
        {
            Item = item;
        }

        /// <summary>
        /// Gets or sets the item.
        /// </summary>
        /// <value>The item.</value>
        [YAXSerializableField]
        [YAXSerializeAs("id")]
        [YAXAttributeForClass]
        public string Item {
            get;
            set;
        }
	}

}

