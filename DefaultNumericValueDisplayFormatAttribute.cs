/*
 * The base library for ProbeNet exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace ProbeNet.Export
{
    /// <summary>
    /// Default numeric value display format attribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class DefaultNumericValueDisplayFormatAttribute: Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.DefaultNumericValueDisplayFormatAttribute"/> class.
        /// </summary>
        /// <param name="format">Format.</param>
        public DefaultNumericValueDisplayFormatAttribute (NumericValueDisplayFormat format)
        {
            Format = format;
        }

        /// <summary>
        /// Gets the format.
        /// </summary>
        /// <value>The format.</value>
        public NumericValueDisplayFormat Format {
            get;
            private set;
        }
    }
}

